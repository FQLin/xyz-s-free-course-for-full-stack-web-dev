import java.util.stream.IntStream;

public class Solution_4 {
    public boolean containsDuplicate(int[] nums){
        return IntStream.of(nums).distinct().count() != nums.length;
    }
}

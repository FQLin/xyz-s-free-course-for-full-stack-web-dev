package ccc;

public class StuManager implements StuInterface{

    private Student students[]=new Student[10];

    public StuManager(){
        students[0]=new Student(1,"张三",'男',18);
        students[1]=new Student(2,"李四",'男',18);

    }
    @Override
    public void selectAll() {
        System.out.println("学生编号\t姓名\t性别\t年龄");
        for (int i = 0; i < students.length; i++) {
            if(students[i]!=null){
                System.out.println(students[i].getId()+"\t"+students[i].getName()+"\t"+students[i].getSex()+"\t"+students[i].getAge());
            }
        }
    }

    @Override
    public void selectByName(String name) {
        System.out.println("学生编号\t姓名\t性别\t年龄");
        for (int i = 0; i < students.length; i++) {
            if(students[i]!=null && students[i].getName().equals(name)){
                System.out.println(students[i].getId()+"\t"+students[i].getName()+"\t"+students[i].getSex()+"\t"+students[i].getAge());
            }
        }
    }

    @Override
    public Student selectById(int id) {
        Student stu=null;
        for (int i = 0; i < students.length; i++) {
            if(students[i]!=null && students[i].getId()==id){
                stu=students[i];
                break;
            }
        }
        return stu;
    }

    @Override
    public boolean addStudent(Student stu) {
        boolean b=false;
        for (int i = 0; i < students.length; i++) {
            if(students[i]==null){
                students[i]=stu;
                b=true;
                break;

            }
        }
        return b;
    }

    @Override
    public boolean editStudent(Student stu) {
        boolean b=false;
        for (int i = 0; i < students.length; i++) {
            if(students[i]!=null && students[i].getId()==stu.getId()){
                students[i].setName(stu.getName());
                students[i].setAge(stu.getAge());
                students[i].setSex(stu.getSex());
                b=true;
                break;
            }
        }
        return b;

    }

    @Override
    public boolean delStudentById(int id) {
        boolean b=false;
        for (int i = 0; i < students.length; i++) {
            if(students[i]!=null && students[i].getId()==id){
                students[i]=null;
                b=true;
                break;
            }
        }
        return b;
    }
}

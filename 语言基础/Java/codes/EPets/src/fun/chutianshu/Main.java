package fun.chutianshu;

public class Main {

    public static void main(String[] args) {
	// write your code here
        //定义10个宠物的宠物数组
        Pet [] pets= new Pet[10];
        //前5个放猫
        for(int i = 0;i<5;i++)
        {
            pets[i] = new Cat();
        }
        //后5只放狗
        for(int i=5;i<pets.length;i++)
        {
            pets[i]=new Dog();
        }
        //一键陪玩
        //激活多态代码：用相同的代码，执行不同的操作
        for(int i=0;i< pets.length;i++){
            pets[i].Play();
        }
    }
}

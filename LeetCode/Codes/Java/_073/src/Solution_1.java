//思路：
// 1.双循环遍历出所有原值为0的索引
// 2. 设置新值为0

import java.util.HashSet;

public class Solution_1 {
    public void setZeroes(int[][] matrix) {
        //用来存储行索引
        HashSet<Integer> r=new HashSet<>();
        //用来存储列索引
        HashSet<Integer> c=new HashSet<>();
        //矩阵行列数
        int m = matrix.length;
        int n=  matrix[0].length;
        for(int i=0;i<m;i++)
        {
            for(int j=0;j<n;j++)
            {
                if(matrix[i][j]==0)
                {
                    r.add(i);
                    c.add(j);
                }
            }
        }
        for (int x: r) {
            for(int i=0;i<n;i++)
            {
                matrix[x][i]=0;
            }
        }
        for(int x:c)
        {
            for(int i=0;i<m;i++)
            {
                matrix[i][x]=0;
            }
        }

    }
}

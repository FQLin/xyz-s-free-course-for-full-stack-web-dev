import java.util.HashMap;
import java.util.Map;

// 思路：
// 用字符ascII编号作为索引新建0数组
public class Solution_2 {
    public int firstUniqChar(String s) {
        char []cs=s.toCharArray();
        char []ans=new char[128];
        for(int i=0;i<cs.length;i++){
            ans[cs[i]]++;
        }
        for(int i=0;i<cs.length;i++){
            if(ans[cs[i]]==1){
                return i;
            }
        }
        return -1;

    }
}

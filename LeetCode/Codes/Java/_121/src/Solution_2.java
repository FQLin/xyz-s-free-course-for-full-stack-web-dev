import java.util.Arrays;

/*
    建立
    算法思路：
    遍历当前最小值和最大利润

    算法分析：
    时间复杂度O(n)
    空间复杂度O(1)

 */
public class Solution_2 {
    public int maxProfit(int[] prices) {
        int minPrice = prices[0];
        int maxProfit = 0;
        for(int i = 1;i<prices.length;i++)
        {
            if(minPrice<prices[i])
            {
                if(maxProfit<prices[i]-minPrice)
                {
                    maxProfit=prices[i]-minPrice;
                }
            }
            else{
                minPrice=prices[i];
            }
        }
        return maxProfit;
    }
}

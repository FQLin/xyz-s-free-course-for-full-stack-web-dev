# HashSet

## 1. java

HashSet 类位于 java.util 包中

![](../imgs/HashSet-1.png)

### 1.1 HashSet 是HashMap的简单封装

```java
public class HashSet<E> implements Set<E> {
    // 持有一个HashMap:
    private HashMap<E, Object> map = new HashMap<>();

    // 放入HashMap的value:
    private static final Object PRESENT = new Object();

    public boolean add(E e) {
        return map.put(e, PRESENT) == null;
    }

    public boolean contains(Object o) {
        return map.containsKey(o);
    }

    public boolean remove(Object o) {
        return map.remove(o) == PRESENT;
    }
}
```

### 1.2 HashSet 特点

* 实现了 Set 接口，不允许有重复元素的集合
* HashSet 允许有 null 值。
* HashSet 是无序的，即不会记录插入的顺序。
* HashSet 不是线程安全的， 如果多个线程尝试同时修改 HashSet，则最终结果是不确定的。 必须在多线程访问时显式同步对 HashSet 的并发访问。

### 1.3 常用方法

1、add(Object obj)：向Set集合中添加元素，添加成功返回true，否则返回false。

2、size()：返回Set集合中的元素个数。

3、remove(Object obj)： 删除Set集合中的元素，删除成功返回true，否则返回false。

4、isEmpty()：如果Set不包含元素，则返回 true ，否则返回false。

5、clear()： 移除此Set中的所有元素。

6、iterator()：返回在此Set中的元素上进行迭代的迭代器。

7、contains(Object o)：如果Set包含指定的元素，则返回 true，否则返回false。

## 2. javascript & TypeScript

js 中，只有set 的概念：

https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Reference/Global_Objects/Set

## 3. Set 集 相关算法

### 3.1 根本原则：“去重”

根据Set的基本特性，达到去重效果

### 3.2 贪心算法

贪婪算法（贪心算法）是指在对问题进行求解时，在每一步选择中都采取最好或者最优（即最有利）的选择，从而希望能够导致结果是最好或者最有的算法。
贪心算法所得到的结果不一定是最优的结果（有时会是最优解），但是都是相对近似最优解的结果
贪心算法最佳应用—集合覆盖

参考资料：[https://blog.nowcoder.net/n/0f2ff8b34bcf43c0b2565724ea30f9eb](https://blog.nowcoder.net/n/0f2ff8b34bcf43c0b2565724ea30f9eb)
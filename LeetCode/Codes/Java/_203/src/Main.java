import java.util.LinkedList;

public class Main {
    public static void main(String[] args) {
        ListNode head = new ListNode(1);
        head.next=new ListNode(2);
        ListNode next = head.next;
        next.next = new ListNode(6);
        next=next.next;
        next.next = new ListNode(3);
        next=next.next;
        next.next = new ListNode(4);
        next=next.next;
        next.next = new ListNode(5);
        next=next.next;
        next.next = new ListNode(6);
        next=next.next;

        Solution_1 solution = new Solution_1();

        next = solution.removeElements(head,6);

        while (next!=null)
        {
            System.out.println(next.val);
            next=next.next;
        }

    }
}
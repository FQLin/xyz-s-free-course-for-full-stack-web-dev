import java.util.ArrayList;
import java.util.List;

// 自定义队列 FIFO
public class MyQueue {
    // 队列中存储数据用到的列表
    private List<Integer> data;
    // 头部索引
    private int p_start;
    public MyQueue(){
        data = new ArrayList<Integer>();
        p_start = 0;
    }
    // 入队
    public boolean enQueue(int x){
        data.add(x);
        return true;
    }
    //出队
    public boolean deQueue(){
        if(data.remove(p_start)!=null){
            p_start++;
            return true;
        }
        return false;
    }

    //获取头元素值
    public int GetFront(){
        return data.get(p_start);
    }

    //队列是否为空
    public boolean IsEmpty(){
        if(data.size()==0)
        {
            return true;
        }
        return false;
    }
}

import java.util.ArrayList;
import java.util.List;

/*
*
* 算法：递归
*
* 需求：给一个根节点，要求进行前序遍历
*
* 思路：
* 1. 对于任意一个节点 node ，resultList = node.val + preorderTraversal(node.left) + preorderTraversal(node.right)
* */
public class Solution_1 {
    public List<Integer> preorderTraversal(TreeNode root) {
        List<Integer> resultList = new ArrayList<>();
        if(root==null)
        {
            return resultList;
        }
        resultList.add(root.val);
        if(root.left!=null) {
            resultList.addAll(preorderTraversal(root.left));
        }
        if(root.right!=null)
        {
            resultList.addAll(preorderTraversal(root.right));
        }
        return resultList;
    }
}

import java.util.Arrays;

public class Solution_3 {
    //最优算法，逆向，类似双指针
    //时间复杂度O(n)，空间复杂度O(1)
    public void merge(int[] nums1, int m, int[] nums2, int n) {
        int len = nums1.length;
        while (n>0)
        {
            if(m>0&&nums1[m-1]>nums2[n-1])
            {
                nums1[--len]= nums1[--m];
            }
            else {
                nums1[--len]=nums2[--n];
            }
        }
    }
}

# javascript 函数 function

## 1. 概念

函数是 JavaScript 中的基本组件之一。

一个函数是 JavaScript 过程 ：一组执行任务或计算值的语句。


## 2. 语法

``` js
function 函数名(参数列表)
{
    函数体内部语句段
}
```

* js 中的函数不需写加返回值类型
* js 中函数可以不写参数类型


> 参考资料：
> [https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Functions](https://developer.mozilla.org/zh-CN/docs/Web/JavaScript/Guide/Functions)

## 3. 回调函数

用作其他函数参数的函数，叫做回调函数。回调函数会根据使用它的函数中语句的具体执行情况，来决定是否被调用

特点：
* 回调函数会以参数的形式传递给调用他的函数
* 回调函数是否执行，要看调用它的函数中代码具体执行情况，可能会被执行，也可能不会被执行
* 回调函数通常用在用户交互操作，以及异步接口调用中

## 4. 匿名函数

匿名函数就是没有函数名的函数，通常用在函数表达式或单次的函数执行中。

``` js
let sum = function(a+b){
    return a+b
}

```

## 5. 箭头函数

箭头函数是匿名函数的语法糖，是匿名函数的简写方式

``` js
//格式1： 单行
// 带参，带返回值
let sum = (a,b) => a+b
// 无参，无返回值
let sayHi = ()=> alert("Hi !!!")

// 格式2： 多行
let sum = (a,b)=>{
    let result = a+b
    return result
}

```

public class Solution_1 {
    public boolean isAnagram(String s, String t) {
        int[] m = new int[26];
        for (int i = 0; i < s.length(); i++) {
            int x = s.charAt(i);
            m[x - 'a']++;
        }
        for (int i = 0; i < t.length(); i++) {
            int x = t.charAt(i);
            m[x - 'a']--;
        }

        for (int i = 0; i < 26; i++) {
            if (m[i] != 0) {
                return false;
            }
        }
        return true;
    }
}

export class Solution{
    constructor() { }
    public containsDuplicate(nums: number[]): boolean {
        let set = new Set()

        for (let n of nums) { 
            if (set.has(n)) { 
                return true
            }
            set.add(n)
        }
        return false
    }
}
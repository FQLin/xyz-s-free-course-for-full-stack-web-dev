/*
    算法: 迭代

    思路：每层入队列，然后翻转其子节点

 */

import java.util.LinkedList;
import java.util.Queue;

public class Solution_2 {
    public TreeNode invertTree(TreeNode root) {

        if(root==null)
        {
            return root;
        }

        Queue<TreeNode> queue= new LinkedList<>();

        queue.add(root);

        while (queue!=null)
        {
            TreeNode node = queue.poll();
            if(node.left!=null)
            {
                queue.offer(node.left);
            }
            if(node.right!=null)
            {
                queue.offer(node.right);
            }
            TreeNode tmp = node.left;
            node.left = node.right;
            node.right = tmp;

        }


        return root;
    }
}

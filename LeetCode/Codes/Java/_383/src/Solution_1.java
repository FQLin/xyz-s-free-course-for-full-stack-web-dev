//思路：
// 1. 因为只可能是26个小写字母，所以可以创建两个新整型数组，来存储两个字符串中出现的字母次数
// 2. 比对两个数组中各项大小，输出结果
public class Solution_1 {
    public boolean canConstruct(String ransomNote, String magazine) {
        int [] m = new int[26];
        int [] n = new int[26];
        for(int i =0;i<ransomNote.length();i++)
        {
            int x = ransomNote.charAt(i);
            m[x-97]++;
        }
        for(int i =0;i<magazine.length();i++)
        {
            int x = magazine.charAt(i);
            n[x-97]++;
        }

        for(int i=0;i<26;i++)
        {
            if(m[i]>n[i])
            {
                return false;
            }
        }
        return true;
    }
}

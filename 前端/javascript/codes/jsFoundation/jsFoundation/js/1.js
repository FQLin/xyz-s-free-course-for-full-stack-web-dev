// var &  let  区别

var str = "hello！"
//函数定义
let myfun = function showMessage(name='无名氏') {
	//定义一个局部变量 str
	// 局部变量只在定义它的 {} 中生效
	// 参数也是局部变量
	// 用弹窗弹出str信息
	alert(str+name);
};
//两种函数的执行方式：
// 1. 程序员将执行代码写在任意位置，就在执行到该行代码时执行
// 2. 程序员并不知道函数的具体执行时间，要根据用户的操作来控制，
// 将函数绑定到用户事件（用户的某个具体操作）上,用户执行操作时，来执行函数——事件函数
// 事件函数要作为用户控件中事件属性的值
// showMessage() 要做为 按钮控件 Button 的 点击事件属性 onclick 的值

myfun();
//showMessage();
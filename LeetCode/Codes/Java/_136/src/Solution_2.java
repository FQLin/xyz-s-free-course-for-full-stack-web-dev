import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;

/*
*
* 使用位运算异或的交换律和结合律
* 同为0，不同为1,0异或所有值，得出就是唯一只出现一次的值
*
* */
public class Solution_2 {
    public int singleNumber(int[] nums) {
        int x = 0;

        for (int n: nums) {
            x^=n;
        }

        return x;
    }
}

package fun.chutianshu;

public class Husky extends Dog implements FireSkill {
    public Husky() { }

    public Husky(String name, String type) {
        super(name, type);
    }


    //覆盖基类中同名方法
    @Override
    public void Play(){
        System.out.println("一只名叫"+this.getName()+"的"+this.getType()+"正在兴致勃勃地拆家~~");
    }


    // 实现接口中抽象方法的方式，和实现抽象类中抽象方法的方式一样
    // 先写一个 @Override
    // 再写一个同名的实体方法
    @Override
    public void Fire() {
        System.out.println("这一只名叫"+this.getName()+"的"+this.getType()+"，正在发射火焰弹!!!!");
    }
}

import java.util.HashMap;

public class Solution_2 {
    public int[] twoSum(int[] nums, int target) {
        //创建用于比对的哈希表
        //key是数组值，value 数组是索引
        HashMap<Integer, Integer> map = new HashMap<Integer, Integer>(nums.length-1);
        //循环比对当前索引值和哈希表内存储于的是否配对
        for (int i = 0; i < nums.length; i++) {
            if (map.containsKey(target - nums[i])) {
                int[] r = { i, map.get(target - nums[i]) };
                return r;
            }else{
                map.put(nums[i], i);
            }
        }
        return null;
    }
}

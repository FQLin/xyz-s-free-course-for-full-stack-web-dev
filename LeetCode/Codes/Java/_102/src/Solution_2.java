/*
* 思路：
* 递归
* 1. 相同层次节点归入同一数组
* 2. 传入辅助的 level 参数决定层次
*
*
*
 */


import java.util.ArrayList;
import java.util.List;

public class Solution_2 {
    List<List<Integer>> resultLists = new ArrayList<>();

    //辅助方法，引入层次值作为参数
    public void helper(TreeNode node,int level)
    {
        //大小等于层次
        if(resultLists.size()==level)
        {
            //新增新的一层
            resultLists.add(new ArrayList<>());
        }

        //当前层，添加值
        resultLists.get(level).add(node.val);

        //左侧分支递归
        if(node.left!=null)
        {
            helper(node.left,level+1);
        }
        //右侧分支递归
        if(node.right!=null)
        {
            helper(node.right,level+1);
        }
    }

    public List<List<Integer>> levelOrder(TreeNode root) {

        if(root==null)
        {
            return resultLists;
        }

        helper(root,0);

        return resultLists;

    }
}

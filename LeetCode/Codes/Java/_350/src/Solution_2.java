import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.stream.Collectors;

/*
  算法思路：使用哈希映射
  1. 创建 HashMap 中，记录一个长度较短的数组中存在的数字和对应出现的次数
  2. 遍历另一个数组，如果存在次数为正，则将匹配到的数字依次放入长度较短的数组中，并记住结果数组长度（索引位）
  3. 根据结果数组长度，返回修改后短数组的子数组（结果数组）

 */
public class Solution_2 {
    public int[] intersect(int[] nums1, int[] nums2) {

        HashMap<Integer, Integer> hashMap = new HashMap<>();

        //选出较小的数组，将引用传递给nums，并不会创建新数组
        int[] numsShort,numsLong;
        if(nums1.length>nums2.length)
        {
            numsShort=nums2;
            numsLong=nums1;
        }
        else {
            numsShort=nums1;
            numsLong=nums2;
        }

        //遍历哈希表，有的加1，没有的填进去
        //完成的哈希表中，会记录去重后较短数组中存在的所有数字及对应出现的次数
        int x=0;
        for (int n:numsShort) {
            int i=1;
            if(hashMap.containsKey(n)) {
                x = hashMap.get(n).intValue();
                hashMap.put(n,++x);
            }
            else {
                hashMap.put(n,i);
            }
        }

        for (Integer key:hashMap.keySet()) {
            System.out.println(key.intValue());
            System.out.println(hashMap.get(key).intValue());
        }

        int k=0;
        for(int i=0;i<numsLong.length;i++)
        {

            if(hashMap.containsKey(numsLong[i]))
            {
                if(hashMap.get(numsLong[i]).intValue()>0)
                {
                    numsShort[k]=numsLong[i];
                    k++;
                    x=hashMap.get(numsLong[i]).intValue();
                    hashMap.put(numsLong[i],--x);
                }
            }
        }


        return Arrays.copyOf(numsShort,k);

    }
}

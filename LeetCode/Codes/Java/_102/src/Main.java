public class Main {
    public static void main(String[] args) {
        /*
        TreeNode root = new TreeNode(3);
        root.left=new TreeNode(9);
        root.right=new TreeNode(20);
        root.right.left=new TreeNode(15);
        root.right.right=new TreeNode(7);
        */
        TreeNode root = new TreeNode(1);
        root.left=new TreeNode(2);

        Solution_1 solution = new Solution_1();
        System.out.println(solution.levelOrder(root));
    }
}
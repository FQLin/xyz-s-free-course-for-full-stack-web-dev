# JDK 源码学习项目搭建

## 1. 下载 JDK

有很多不同版本的 JDK ，虽然 java 最早是由 Sun 公司发明，并后来被 Oracle 公司收购，虽然 java 开源，但 Oracle 的 jdk 分为两个版本，一个完全免费，一个需要商业许可。

而 OpenJDK 的开源也是完全免费的。

这两个版本的 JDK 是市面上使用最广的。

很多公司为了避免商业纠纷，并且个性化开发，就基于完全开源的 JDK 开发出自己公司的开源 JDK，各有一些区别。

在当前这个学习路径下，我介绍 Oracle 的 OpenJDK （完全开源并免费），以及 OpenJDK 的最新版 - 19（2023-1月），并以 Oracle 的为主。


### 1.1 下载 安装 Oracle JDK 19

到 Oracle 官方网站上找最新的 19 版本，选择适合自己操作系统的安装包

下面链接中是 Windows 的

https://www.oracle.com/java/technologies/downloads/#jdk19-windows

![](../../imgs/oracleJdk-download.png)

### 1.2 下载 OpenJDK 19

OpenJDK 源码可以直接从 github 中获取

https://github.com/openjdk/jdk

![](../../imgs/openjdk_git.png)

## 2. 找 JDK 源码

### 2.1 Oracle JDK 19

以 Window 10 为例，Oracle 的 JDK 19 源码包位于安装路径下的：java\jdk-19\lib\src.zip 中，解压到合适的路径下，用 ide 打开即可

![](../../imgs/oracle_jdk_src.png)

### 2.2 OpenJDK 19

直接使用 git clone 将 github 上的代码克隆到本地指定地址

``` shell
git clone https://github.com/openjdk/jdk.git
```
![](../../imgs/openjdk_download.png)

## 3. 读代码

使用 IDE 打开需要研读的源码文件夹即可，注意，openjdk 也可以选择只打开 src 文件夹

我使用的 IDE 是 idea ，自然，使用 Eclipse 也是可以的

Oracle jdk：

![](../../imgs/oracle_jdk_src1.png)

Open jdk：  
![](../../imgs/openjdk_src.png)
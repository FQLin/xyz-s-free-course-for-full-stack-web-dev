import java.util.*;
import java.util.stream.Collectors;
/*
    算法思路：
    1. 将数组转换为 ArrayList
    2. 通过 ArrayList 的 add 、 remove 方法，借助双循环，将相同的数据存入新列表中

    算法分析：
    时间复杂度 O(n2)，空间复杂度 O(3n)，两者都很差，不建议使用

 */
public class Solution_1 {
    public int[] intersect(int[] nums1, int[] nums2) {
        /*ArrayList nums3List= new ArrayList();
        int i=0,j=0;
        for(;i<nums1.length;i++)
        {
            for(;j<nums2.length;j++) {
                if (nums1[i] == nums2[j])
                {
                    nums3List.add(nums1[i]);
                }
            }
        }

        return nums3List.stream().mapToInt(x-> (int) x).toArray();*/
        List<Integer> nums1List =  Arrays.stream(nums1).boxed().collect(Collectors.toList());
        List<Integer> nums2List =  Arrays.stream(nums2).boxed().collect(Collectors.toList());
        List<Integer> nums3List= new ArrayList();
        for(int i =0;i<nums1List.size();i++){
            for(int j =0;j<nums2List.size();j++)
            {
                if(i>=0&&j>=0&&nums1List.get(i).intValue()==nums2List.get(j).intValue())
                {
                    nums3List.add(nums1List.get(i));
                    nums1List.remove(i);
                    nums2List.remove(j);
                    i--;
                    j--;
                }

            }
        }
        return nums3List.stream().mapToInt(x-> (int) x).toArray();

    }
}
